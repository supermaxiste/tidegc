# Parameters for the pipeline:
      #path_to_file
      #output_path
      #time_points
      #time_points_names
      #conditions
      #conditions_names
      #number_of_samples #order must follow condition names or (if 1 condition) time points names
      #dim1
      #dim2
      #gene_threshold

#Inputs

batch_mode <- F # do not modify this
path_to_file <- "~/Pathway/counts.csv"
output_path <- "~/Pathway/outputFolder"
time_points <- # for example 32
time_points_names <- c(name1, name2)
conditions <- # for example 3
conditions_names <- c(name1, name2)
number_of_samples <- # for example c(2,2,2,2)
dim1 <- 5
dim2 <- 5     #dim 1 and 2 provide the size of the grid of SOM
              #the number of clusters is given by dim1*dim2
              #normally it's better to use a square
gene_threshold <- 2500 #threshold of high variance genes to select

################################# TiDeGC #################################
#
# Command line arguments
#
Version = '20170601-092821'
args <- commandArgs(trailingOnly = T)
if(length(args) < 1 && batch_mode){
  cat("Command line usage:
        Rscript Pipeline.r [count.csv] (output_path)\n")
  q()
}
cat("Script version", Version, "\n")
cat(R.version.string, "\n")
cat("Required packages:\n")
cat(paste("\tDESeq2 version", packageVersion("DESeq2")), "\n")
cat(paste("\tFlowSOM version", packageVersion("FlowSOM")), "\n")
if(!is.na(args[1])){
  path_to_file <- args[1]
}
if(!is.na(args[2])){
  output_path <- args[2]
}
if(!file.exists(output_path)){
  dir.create(output_path)
}

#
# parameter check
#
cat("Parameters:\n")
cat("\tpath_to_file:", path_to_file, "\n")
cat("\toutput_path:", output_path, "\n")
cat("\ttime_points:", time_points, "\n")
cat("\ttime_points_names:", time_points_names, "\n")
cat("\tconditions:", conditions, "\n")
cat("\tconditions_names:", conditions_names, "\n")
cat("\tnumber_of_samples:", number_of_samples, "\n")

# 
# load libraries
#
cat("Loading libraries...\n")
library(DESeq2)
library(FlowSOM)

#
# functions
#

DeSeqMatrix <- function(conditions_names, time_points_names, number_of_samples, data_tot){
  if (length(conditions_names)>=2){
    # colDat is a data.frame describing our samples with the column 
    # names representing factors and row names representing our 
    # different samples
    
    colDat <- data.frame(cbind(c(rep(rep(conditions_names, each=length(time_points_names)), number_of_samples)), c(rep(rep(time_points_names, length(conditions_names)), number_of_samples))))
    colnames(colDat) <- c("condition", "time")
    rownames(colDat) <- colnames(data_tot[3:length(data_tot[1,])])
    
    # condition is a vector with 3 integers according to their condition
    # NOTE: the 3 integers are factors and NOT integers
    # With DESeqDataSetFromMatrix we specify tidy=TRUE so that the first
    # column of data_tot is used for naming rows
    
    condition <- c(rep(conditions_names, each=length(time_points_names)))
    time <- c(rep(time_points_names, length(conditions_names)))
    dds <- DESeqDataSetFromMatrix(countData = data_tot[c(1,3:length(data_tot[1,]))],
                                  colData = colDat,
                                  design = ~condition + time,
                                  tidy=TRUE)
  }
  else{
    colDat <- data.frame(rep(time_points_names, number_of_samples))
    colnames(colDat) <- c("time")
    rownames(colDat) <- colnames(data_tot[3:length(data_tot[1,])])
    time <- c(rep(time_points_names, length(conditions_names)))
    dds <- DESeqDataSetFromMatrix(countData = data_tot[c(1,3:length(data_tot[1,]))],
                                  colData = colDat,
                                  design = ~time,
                                  tidy=TRUE)
  }
  return(dds)
}

# Removing genes with counts=0 over all times

prefiltering <- function(dds){
  dds <- dds[ rowSums(counts(dds)) > 1, ]
  return(dds)
}

#rlog or vst transformation of the data (includes normalization)

countTransform <- function(dds, number_of_samples){
  dds <- estimateSizeFactors(dds)
  if (sum(number_of_samples)>=30){
    rld <- vst(dds, blind = FALSE)
  }
  else{
    rld <- rlog(dds, blind=FALSE)
  }
  return(rld)
}

# Averaging for every time point for rlog transformed data

averageRlog <- function(number_of_samples, rld){
  rld2 <- NULL
  average_ending <- cumsum(number_of_samples)
  average_beginning <- c(1, average_ending+1)
  
  # We use a beginning and ending value to create the ranges of columns
  # for which we apply a mean at each row
  
  for (i in 1:length(average_ending)){
    rld2 <- cbind(rld2, apply(assay(rld)[,average_beginning[i]:average_ending[i]], 1, mean))
  }
  
  
  #Renaming the columns and the rows like in the original rld variable.
  
  colnames(rld2) <- dimensions
  rownames(rld2) <- rownames(rld)
  return(rld2)
}

#Exploratory Analysis

heat <- function(rld2){
  # Heatmap
  sampleDists <- dist(t(rld2))
  library("pheatmap")
  library("RColorBrewer")
  sampleDistMatrix <- as.matrix( sampleDists )
  colnames(sampleDistMatrix) <- NULL
  colors <- colorRampPalette( rev(brewer.pal(9, "Blues")) )(255)
  png(filename="heatmap.png")
  pheatmap(sampleDistMatrix,
           clustering_distance_rows=sampleDists,
           clustering_distance_cols=sampleDists,
           col=colors)
  dev.off()
}

# PCA analysis

PrCA <- function(rld2, conditions_names, time_points_names){
  PCA <- prcomp(as.data.frame(t(rld2)))
  sumPCA <- summary(PCA)
  var1 <- round(sumPCA$importance[3,1]*100, 0)
  var2 <- round((sumPCA$importance[3,2]-sumPCA$importance[3,1])*100, 0)
  condition_legend <- rainbow(length(conditions_names))
  condition_colours <- rep(condition_legend, length(time_points_names), each=length(time_points_names))
  times_legend <- rainbow(length(time_points_names))
  times_colours <- rep(times_legend, length(conditions_names))
  png(filename="PCACondition.png")
  plot(PCA$x[,1:2], col = c(condition_colours), xlab=paste("PC1:", var1, "% variance"), ylab=paste("PC2:", var2, "% variance"), pch=19)
  legend("left", c(conditions_names), fill=c(condition_legend))
  dev.off()
  png(filename = "PCATime.png")
  plot(PCA$x[,1:2], col = c(times_colours), xlab=paste("PC1:", var1, "% variance"), ylab=paste("PC2:", var2, "% variance"), pch=19)
  legend("left", c(time_points_names), fill=c(times_legend))
  dev.off()
}


# Selecting high variance genes requires "genefilter" package (2500 default)

highVarGenes <- function(rld2, gene_threshold){
  library("genefilter")
  topVarGenes <- head(order(rowVars(rld2), decreasing = TRUE), gene_threshold)
  mat  <- rld2[ topVarGenes, ]
  return(mat)
}

# We adjust the expression value with the average for each gene

adjust <- function(mat){
  mat  <- mat - rowMeans(mat)
  return(mat)
}

# Clustering can be applied by using a seed in order to have consistency even if
# The FLOW-SOM algorithm is random.

SOMClustering <- function(mat, dim1, dim2){
  set.seed(1)
  counts_matrix <- mat
  flowsom_som25 <- FlowSOM::SOM(counts_matrix, xdim = dim1, ydim = dim2)
  flowsom_codes25 <- flowsom_som25$codes
  rownames(flowsom_codes25) <- 1:nrow(flowsom_codes25)
  return(list(flowsom_codes25, flowsom_som25))
}

# Plotting all the clusters with each gene being a green line and the
# Average being a black line (no average is given for 1 or 0 genes)

plotSOM <- function(flowsom_codes25, flowsom_som25, dimensions, mat, time_points_names, conditions_names, dim1, dim2){
  
  cluster_rows25 <- hclust(dist(flowsom_codes25), method = "average")
  days <- c(1:length(dimensions))
  par(bg = "white")
  cluster_number <- dim1*dim2
  for (i in 1:cluster_number){
    interest_genes <- which(flowsom_som25$mapping[,1]==i)
    interest_genes <- mat[interest_genes,]
    plotname <- paste("Cluster", i, sep="_")
    png(filename=paste(plotname,".png", sep=""))
    #Next condition is used for clusters that are empty
    if(length(interest_genes)>0){
      #Next condition is used for clusters that have only one gene
      if(length(interest_genes)>length(dimensions)){
        plot(days, interest_genes[1,], xlab="", ylab="Counts", xaxt="n", type="l", ylim=c(min(mat),max(mat)), main=paste("Cluster", i))
        for(i in 1:nrow(interest_genes)){
          points(days, interest_genes[i,], type="l", col="green")
        }
        average <- apply(interest_genes, 2, mean)
        points(days, average, type="l", col="black", pch=19)
        axis(1, days, c(rep(time_points_names, length(conditions_names))))
        ycoord <- rep(3, length(conditions_names))
        xcoord <- mean(c(1,length(days)/length(conditions_names)))
        for (i in 1:(length(conditions_names)-1)){
          tail <- tail(xcoord, n=1)+length(time_points_names)
          xcoord <- c(xcoord, tail)
        }
        mtext(text=c(conditions_names), cex=1.5,
              side = 1, line = ycoord, at = xcoord)
        polygon_y <- c(rep(min(mat),2), rep(max(mat), 2))
        #In the case of a single condition, no white spaces have to be drawn
        if (length(conditions_names)>1){
          #The following condition puts white polygons to separate based on the number of conditions if
          #there are more conditions than timepoints
          if(length(conditions_names)>length(time_points_names)){
            for (i in 1:(length(conditions_names)-1)){
              polygon_x <- c(length(conditions_names)*i, length(conditions_names)*i+1, length(conditions_names)*i+1, length(conditions_names)*i)
              polygon(polygon_x, polygon_y, col="white", border = NA)
            }
          }
          #If timepoints are more than conditions, then draw white polygons based on their amount
          else{
            for (i in 1:(length(conditions_names)-1)){
              polygon_x <- c(length(time_points_names)*i, length(time_points_names)*i+1, length(time_points_names)*i+1, length(time_points_names)*i)
              polygon(polygon_x, polygon_y, col="white", border = NA)
            }
          }
        }
        dev.off()
      }
      #Plotting one gene
      else{
        plot(days, interest_genes, xlab="", ylab="Counts", xaxt="n", type="l", col="white", ylim=c(min(mat),max(mat)), main=paste("Cluster", i))
        axis(1, days, c(rep(time_points_names, length(conditions_names))))
        ycoord <- rep(3, length(conditions_names))
        xcoord <- mean(c(1,length(days)/length(conditions_names)))
        for (i in 1:(length(conditions_names)-1)){
          tail <- tail(xcoord, n=1)+length(time_points_names)
          xcoord <- c(xcoord, tail)
        }
        mtext(text=c(conditions_names), cex=1.5,
              side = 1, line = ycoord, at = xcoord)
        polygon_y <- c(rep(min(mat),2), rep(max(mat), 2))
        if (length(conditions_names)>1){
          if(length(conditions_names)>length(time_points_names)){
            for (i in 1:(length(conditions_names)-1)){
              polygon_x <- c(length(conditions_names)*i, length(conditions_names)*i+1, length(conditions_names)*i+1, length(conditions_names)*i)
              polygon(polygon_x, polygon_y, col="white", border = NA)
            }
          }
          else{
            for (i in 1:(length(conditions_names)-1)){
              polygon_x <- c(length(time_points_names)*i, length(time_points_names)*i+1, length(time_points_names)*i+1, length(time_points_names)*i)
              polygon(polygon_x, polygon_y, col="white", border = NA)
            }
          }
        }
      }
    }
    #Plotting 0 genes
    else{
      plotname <- paste("Cluster", i, sep="_")
      png(filename=paste(plotname,".png", sep=""))
      plot(days, rep(0, length(dimensions)), xlab="", ylab="Counts", xaxt="n", type="l", col="white", ylim=c(min(mat),max(mat)), main=paste("Cluster", i))
      axis(1, days, c(rep(time_points_names, length(conditions_names))))
      ycoord <- rep(3, length(conditions_names))
      xcoord <- mean(c(1,length(days)/length(conditions_names)))
      for (i in 1:(length(conditions_names)-1)){
        tail <- tail(xcoord, n=1)+length(time_points_names)
        xcoord <- c(xcoord, tail)
      }
      mtext(text=c(conditions_names), cex=1.5,
            side = 1, line = ycoord, at = xcoord)
      dev.off()
    }
  }
}

geneSOM <- function(flowsom_som25, mat, data_tot, dim1, dim2){         
  # Output the 25 lists of genes
  
  cluster_number <- dim1*dim2
  cluster_homolog <- c(rep(NA,cluster_number))
  for (i in 1:cluster_number){
    interest_genes <- which(flowsom_som25$mapping[,1]==i)
    interest_genes <- mat[interest_genes,]
    ATID <- data_tot[match(rownames(interest_genes), data_tot[,1]),2]
    ATID <- ATID[!is.na(ATID)]
    cluster_homolog[i] <- length(ATID)
    file_name <- paste(paste("Cluster",i, sep="_"), "genes.txt", sep="_")
    write(t(as.matrix(ATID)), file=file_name, ncolumns = 1)
  }
  cluster_size <- numbers(flowsom_som25, mat, data_tot, dim1, dim2)
  percentage_tot <- round((cluster_homolog/cluster_size)*100, 0)
  summary<-file("summary.txt")
  write(c("General Statistics\n"), file="summary.txt", append = TRUE)
  write(paste(paste(c("Average percentage of homolog genes per cluster: "), mean(percentage_tot)), "%", sep = ""), file="summary.txt", append=TRUE)
  write(paste(c("Average number of genes per cluster: "), mean(cluster_size)), file="summary.txt", append=TRUE)
  write(paste(c("Median number of genes per cluster: "), median(cluster_size)), file="summary.txt", append=TRUE)
  write(paste(c("Cluster with highest amount of genes: "), which(cluster_size==max(cluster_size))), file="summary.txt", append=TRUE)
  write(paste(c("Cluster with lowest amount of genes: "), which(cluster_size==min(cluster_size))), file="summary.txt", append=TRUE)
  write(c("\nCluster details"), file="summary.txt", append=TRUE)
  for (i in 1:cluster_number){
  write(paste(c("\nCluster"), i, sep = " "), file="summary.txt", append=TRUE)
  write(paste(c("Number of genes in the cluster: "), cluster_size[i]), file="summary.txt", append=TRUE)
  write(paste(c("Number of genes with a homolog: "), cluster_homolog[i]), file="summary.txt", append=TRUE)
  write(paste(paste(c("Percentage of genes with a homolog: "), round((cluster_homolog[i]/cluster_size[i])*100, 0)), "%"), file="summary.txt", append=TRUE)
  }
  close(summary)
}

numbers <- function(flowsom_som25, mat, data_tot, dim1, dim2){
  cluster_number <- dim1*dim2
  cluster_size <- c(rep(NA,cluster_number))
  for (i in 1:cluster_number){
    interest_genes <- which(flowsom_som25$mapping[,1]==i)
    cluster_size[i] <- length(interest_genes)
  }
  return(cluster_size)
}

#
# main
#

# Loading count data (needs .txt/csv difference)

data_tot <- read.table(path_to_file, sep=";", header=T)

# Setting working directory

setwd(output_path)

# Creating column names from time and condition names

dimensions <- as.vector(outer(time_points_names, conditions_names, paste, sep="_"))

#Pipeline example

dds <- DeSeqMatrix(conditions_names, time_points_names, number_of_samples, data_tot)
dds <- prefiltering(dds)
rld <- countTransform(dds, number_of_samples)
rld2 <- averageRlog(number_of_samples, rld)
heat(rld2)
PrCA(rld2, conditions_names, time_points_names)
mat <- highVarGenes(rld2, gene_threshold)
mat <- adjust(mat)
flowsom_25 <- SOMClustering(mat, dim1, dim2)
plotSOM(flowsom_25[[1]], flowsom_25[[2]], dimensions, mat, time_points_names, conditions_names, dim1, dim2)
geneSOM(flowsom_25[[2]], mat, data_tot, dim1, dim2)
