

### What is this repository for? ###

The repository includes the following:  
	- The TiDeGC R code    
	- A file with *C. flexuosa* raw counts  
	- A file with *M. bancana* raw counts  
	- The master thesis behind TiDeGC that includes step-by-step descriptions  
	- PowerPoint Presentation files

### What is TiDeGC? ###

TiDeGC stands for Time Dependent Gene Clustering. TiDeGC is a pipeline developed 
in R that clusters gene expression over time with RNA-seq raw counts data.

### How do I use TiDeGC? ###

TiDeGC requires simple inputs related to the experimental design behind the 
dataset of interest. For details please refer to the Master Thesis included
in this repository.

### Who developed TiDeGC? ###

I did (with great help) :) I'm Stefan Milosavljevic, a master student in Computational Biology
and Bioinformatics and with the help of two amazing supervisors: Masaomi Hatakeyama and Reiko
Akiyama, I managed to create this pipeline in 6 months. We're part of Kentaro Shimizu's lab, 
where the focus is studying evolution with the help of genomic data.
Oh and we are all located in Zurich, Switzerland!

### Who do I contact for help? ###

Look for the people mentioned before or write to me directly at: stefan.milos.srb.ch@gmail.com
